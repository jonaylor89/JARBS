# JARBS
John's Auto-Ricing Bootstrapping script

--------------------

# Deploying

```sh
~$ curl -sS https://raw.githubusercontent.com/jonaylor89/JARBS/master/jarbs.sh | bash
```

------------------

# TODO

* Differentiate between Arch based, Debian based, macOS, and maybe OpenSUSE/Fedora
  - `arch.sh`, `debian.sh`, `macOS.sh`
