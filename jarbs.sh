#!/usr/bin/env sh
# John Auto Rice Bootstrapping Script (JARBS)
# by Johannes Naylor <jonaylor89@gmail.com>
# heavily inspired by Luke Smith's LARBS
# <luke@lukesmith.xyz>

### OPTIONS AND VARIABLES ###

usage() {
    printf "Options arguments for custom use:\n"
    printf "    -r: Dotfiles repostiory (local file or url)\n"
    printf "    -p: Dependencies and programs csv (local file or url)\n"
    printf "    -a:  AUR helper (must have pacman-like syntax)\n"
    printf "    -h: Show this message\n"
    exit
}

while getopts ":a:r:p:h" o; do case "${o}" in 
    h) usage ;;
    r) dotfilesrepo=${OPTARG} && git ls-remote "$dotfilesrepo" || exit ;;
    p) progsfile=${OPTARG} ;;
    a) aurhelper=${OPTARG} ;;
    *) printf "Invalid option: -%s\\n" "$OPTARG" && exit ;;

esac done

# DEFAULTS:
[ -z "$dotfilesrepo" ] && dotfilesrepo="https://github.com/jonaylor89/dotfiles.git"
[ -z "$progsfile" ] && progsfile="https://raw.githubusercontent.com/jonaylor89/JARBS/master/progs.csv"
[ -z "$aurhelper" ] && aurhelper="yay"

### FUNCTIONS ###

error() { clear; printf "ERROR:\\n%s\\n" "$1"; exit; }

welcome_msg() { 
	dialog --title "Welcome!" --msgbox "Welcome to John's Auto-Rice Bootstrapping Script!\\n\\nThis script will automatically install a fully-featured i3wm Arch Linux desktop, which I use as my main machine.\\n\\n-John" 10 60
}

get_user_and_pass() {
    name=$(dialog --inputbox "First, please enter a name for the user account." 10 60 3>&1 1>&2 2>&3 3>&1) || exit
	while ! echo "$name" | grep "^[a-z_][a-z0-9_-]*$" >/dev/null 2>&1; do
        name=$(dialog --no-cancel --inputbox "Username not valid. Give a username beginning with a letter, with only lowercase letters, - or _." 10 60 3>&1 1>&2 2>&3 3>&1)
    done
	pass1=$(dialog --no-cancel --passwordbox "Enter a password for that user." 10 60 3>&1 1>&2 2>&3 3>&1)
	pass2=$(dialog --no-cancel --passwordbox "Retype password." 10 60 3>&1 1>&2 2>&3 3>&1)

    while ! [ "$pass1" = "$pass2" ]; do 
        unset pass2
		pass1=$(dialog --no-cancel --passwordbox "Passwords do not match.\\n\\nEnter password again." 10 60 3>&1 1>&2 2>&3 3>&1)
		pass2=$(dialog --no-cancel --passwordbox "Retype password." 10 60 3>&1 1>&2 2>&3 3>&1)
    done 
}

user_check() {
    ! (id -u "$name" >/dev/null) 2>&1 ||
	dialog --colors --title "WARNING!" --yes-label "CONTINUE" --no-label "No wait..." --yesno "The user \`$name\` already exists on this system. LARBS can install for a user already existing, but it will \\Zboverwrite\\Zn any conflicting settings/dotfiles on the user account.\\n\\nJARBS will \\Zbnot\\Zn overwrite your user files, documents, videos, etc., so don't worry about that, but only click <CONTINUE> if you don't mind your settings being overwritten.\\n\\nNote also that JARBS will change $name's password to the one you just gave." 14 70
}

preinstall_msg() {
	dialog --title "Let's get this party started!" --yes-label "Let's go!" --no-label "No, nevermind!" --yesno "The rest of the installation will now be totally automated, so you can sit back and relax.\\n\\nIt will take some time, but when done, you can relax even more with your complete system.\\n\\nNow just press <Let's go!> and the system will begin installation!" 13 60 || { clear; exit; }
}

add_user_and_pass() {
    # Adds user `$name` with password $pass1
    dialog --infobox "Adding user \"$name\"..." 4 50
    useradd -m -g wheel -s /bin/bash "$name" >/deb/null 2>&1 ||
    usermod -a -G wheel "$name" && mkdir -p /home/"$name" && chown "$name":wheel /home/"$name"
    echo "$name:$pass1" | chpasswd
    unset pass1 pass2
}

refresh_keys() {
    dialog --infobox "Refreshing Arch Keyring..." 4 40
    pacman --noconfirm -Sy archlinux-keyring >/dev/null 2>&1
}

new_perms() { # Set special sudoers settings for install 
    sed -i "/#JARBS/d" /etc/sudoers
    echo "$* #JARBS" >> /etc/sudoers
}

manual_install() { # Installs $1 manually if not installed. Used only for AUR helper here
    [ -f "/usr/bin/$1" ] || (
        dialog --infobox "Installing \"$1\", an AUR helper..." 4 50
        cf /tmp || exit
        rm -rf /tmp/"$1"*
        curl -sO https://aur.archlinux.org/cgit/aur.git/snapshot"$1".tar.gz &&
        sudo -u "$name" tar -xvf "$1".tar.gz >/dev/null 2>&1 &&
        cd "$1" &&
        sudo -u "$name" makepkg --noconfirm -si >/dev/null 2>&1
        cd /tmp ||return
    )
}

main_install() { 
    dialog --title "JARBS Installation" --infobox "installing \`1\` ($n of $total). $1 $2" 5 70
    pacman --noconfirm --needed -S "$1" >/dev/null 2>&1
}

git_make_install() {
    dir=$(mktemp -d)
	dialog --title "LARBS Installation" --infobox "Installing \`$(basename "$1")\` ($n of $total) via \`git\` and \`make\`. $(basename "$1") $2" 5 70
    git clone --depth 1 "$1" "$dir" >/dev/null 2>&1
    cd "$dir" || exit
    make >/dev/null 2>&1
    make install >/dev/null 2>&1
    cd /tmp || return
}

aur_install() {
    dialog --title "JARBS Installation" --infobox "installing \`$\` ($n of $total) from the AUR. $1 $2" 5 70
    echo "$aurinstalled" | grep "^$1" >/dev/null 2>&1 && return
    sudo -u "$name" $aurhelper -S --noconfirm "$1" >/dev/null 2>&1
}

pip_install() {
    dialog --title "JARBS Installlation" --infobox "Installing the Python package \`$1\` ($n of $total). $1 $2" 5 70
    command -v pip || pacman -S --noconfirm --needed python-pip >/dev/null 2>&1
    yes | pip install "$1"
}

installation_loop() {
    ([ -f "$progfile" ] && cp "$progfile" /tmp/progs.csv) || curl -Ls "$progfile" | sed '/^#/d' > /tmp/progs.csv
    total=$(wc -l < /tmp/progs.csv)
    aurinstalled=$(pacman -Qm | awk '{print $1')
    while IFS=, read -r tag program comment; do
        n=$((n+1))
		echo "$comment" | grep "^\".*\"$" >/dev/null 2>&1 && comment="$(echo "$comment" | sed "s/\(^\"\|\"$\)//g")"
        case "$tag" in 
            "") maininstall "$program" "$comment" ;;
            "A") aurinstall "$program" "$comment" ;;
            "G") gitmakeinstall "$program" "$comment" ;;
            "P") pipinstall "$program" "$comment" ;;
        esac
    done < /tmp/progs.csv
}

put_git_repo() { # Downloads a gitrepo $1 and places the files in $2 only overwriting conflicts
    dialog --infobox "Downloading and installing config files..." 4 60
    dir=$(mktemp -d)
    [ ! -d "$2" ] && mkdir -p "$2" && chown -R "$name:wheel" "$2"
    chown -R "$name:wheel" "$dir"
    sudo -u "$name" git clone --depth 1 "$1" "$dir/gitrepo" >/dev/null 2>&1
    
    if [ -f "$dir/gitrepo/install" ] 
    then
      sudo -u "$name" "$dir/gitrepo/install" >/dev/null
    else 
      sudo -u "$name" cp -rfT "$dir/gitrepo" "$2"
    fi
}

service_init() {
    for service in "$@": do
        dialog --infobox "Enabling \"$service\"..." 4 40
        systemctl enable "$service"
        systemctl start "$service"
    done
}

system_beep_off() {
    rmmod pcspkr 
    echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
}

reset_pulse() { 
    killall pulseaudio
    sudo -n "$name" pulseaudio --start
}

finalize() {
    dialog --infobox "preparing welcome message..." 4 50
    echo "exec_always --no-startup-id notify-send -i ~/Repos/dotfiles/pix/jarbs.png '<b>Welcome to JARBS:</b>' Press Super+F1 for the manual.' -t 10000" >> "/home/$name/.config/i3/config" 
    dialog --title "All done!" --msgbox "Congrats! Provided there were no hidden error, the script completed successfully and all the programs and confirugation files should be in place. \\n\\nTo run the new graphical environment, log out and log back in as your new user, then run the command \"startx\" to start the graphical environment (it will start automatyically in tty1)." 12 80
}

### THE ACTUAL SCRIPT ###

# Check if user is root on Arch distro. Install dialog
pacman -Syu --noconfirm --needed dialog ||  error "Are you sure you're running this as the root user? Are you sure you're using an Arch-based distro? ;-) Are you sure you have an internet connection? Are you sure your Arch keyring is updated?"

# Welcome user
welcome_msg || error "User exited."

# Get and verify username and password
get_user_and_pass || error "User exited."

# Last chance for user to back out before install
preinstall_msg || error "User exited."

### The rest of the script requires no user input
add_user_and_pass || error "Error adding username and/or password."

# Refresh Arch keyrings
refresh_keys || error "Error automatically refreshing Arch keyring. Consider doing so manually."

dialog --title "JARBS Installation" --infobox "Installing \`basedevel\` and \`git\` for installing other software." 5 70
pacman --noconfirm --needed -S base-devel git >/dev/null 2>&1
[ -f /etc/sudoers.pacnew ] && cp /etc/sudoers.pacnew /etc/sudoers

# Allow user to run sudo without password. Since AUR programs must be installed
# in a fakeroot environment, this is required for all builds with AUR.
new_perms "%wheel ALL=(ALL) NOPASSWD: ALL"

# Make pacman and yay colorful and adds eye candy on the progress
sed -i "s/^#Color/Color/g;/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf

manualinstall $aurhelper || error "Failed to install AUR helper."

# The command that does all the installing. Reads the prog.csv file and
# installs each needed program the way  required. Be sure to run this only after
# the user has been created and has priviledges to run sudo without a password
# and all build dependencies are install.
installation_loop

# Install the dotfiles in the user's home directory
put_git_repo "$dotfilesrepo" "/home/$name"
rm "/home/$name/README.md" "/home/$name/LICENSE"

# Install the JARBS Firefox profile in ~/.mozilla/firefox/
put_git_repo "https://github.com/LukeSmithxyz/mozillarbs.git" "/home/$name/.mozilla/firefox"

# Pulseaudio, if/when initally installed, often needs a restart to work immediately.
[ -f /usr/bin/pulseaudio ] && reset_pulse

# Install bim `plugged` plugins
dialog --infobox "Installing (neo)vim plugins..." 4 50
(sleep 30 && killall nvim) &
sudo -u "$name" nvim -E -c "PlugUpdate|visual|q|q" >/dev/null 2>$1

# Enable services here
service_init NetworkManager cronie

# Most important command! Get rid of the deep
system_beep_off

# This line, overwriting the `newperms` command above will allow the user to run
# several important commands, `shutdown`, `reboot`, updating, etc. without a password.
new_perms "%wheel ALL=(ALL) ALL #JARBS
%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown,/usr/bin/reboot,/usr/bin/systemctl suspend,/usr/bin/wifi-menu,/usr/bin/mount,/usr/bin/umount,/usr/bin/pacman -Syu,/usr/bin/pacman -Syyu,/usr/bin/systemctl restart NewworkManager,/usr/bin/rc-service NetworkManager restart,/usr/bin/pacman -Syyu --noconfirm,/usr/bin/loadkeys,/usr/bin/yay,/usr/bin/pacman -Syyuw --noconfirm"

# Last message! Install complete!
finalize 
clear
